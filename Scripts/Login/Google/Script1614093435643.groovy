import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startExistingApplication('com.byu.id')

Mobile.waitForElementPresent(findTestObject('Eng/Login/Google/Text Have an account Login'), 10)

Mobile.takeScreenshot(FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Eng/Login/Google/Text Have an account Login'), 0)

Mobile.waitForElementPresent(findTestObject('Eng/Login/Google/LogoGoogle'), 2)

Mobile.takeScreenshot(FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Eng/Login/Google/LogoGoogle'), 0)

Mobile.waitForElementPresent(findTestObject('Eng/Login/Google/Emailgoogle'), 5)

Mobile.takeScreenshot(FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Eng/Login/Google/Emailgoogle'), 0)

Mobile.setText(findTestObject('Eng/Login/Google/Emailgoogle'), GlobalVariable.usernameGoogle, 0)

Mobile.hideKeyboard()

Mobile.waitForElementPresent(findTestObject('Eng/Login/Google/Button Next Password'), 2)

Mobile.takeScreenshot(FailureHandling.CONTINUE_ON_FAILURE)

if (Mobile.waitForElementPresent(findTestObject('Eng/Login/Google/Button Next Password'), 5) == true) {
    Mobile.tap(findTestObject('Eng/Login/Google/Button Next Password'), 0)
} else {
    Mobile.tap(findTestObject('Eng/Login/Google/Berikutmail'), 0)
}

Mobile.waitForElementPresent(findTestObject('Eng/Login/Google/EditText Enter Password'), 5)

Mobile.takeScreenshot(FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Eng/Login/Google/Passwordmail'), 0)

Mobile.setText(findTestObject('Eng/Login/Google/Passwordmail'), GlobalVariable.passwordGoogle, 0)

Mobile.waitForElementPresent(findTestObject('Eng/Login/Google/Passwordmail'), 5)

Mobile.hideKeyboard()

Mobile.waitForElementPresent(findTestObject('Eng/Login/Google/Button Next Login'), 3)

Mobile.takeScreenshot(FailureHandling.CONTINUE_ON_FAILURE)

if (Mobile.waitForElementPresent(findTestObject('Eng/Login/Google/Button Next Login'), 5) == true) {
    Mobile.tap(findTestObject('Eng/Login/Google/Button Next Login'), 0)
} else {
    Mobile.tap(findTestObject('Eng/Login/Google/berikutpass'), 0)
}

Mobile.waitForElementPresent(findTestObject('Eng/TextView My Plan'), 10)

Mobile.takeScreenshot(FailureHandling.CONTINUE_ON_FAILURE)

