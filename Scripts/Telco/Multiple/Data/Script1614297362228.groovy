import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Eng/irenew/Data/Text Buy Data'), 0)

Mobile.waitForElementPresent(findTestObject('Eng/irenew/Data/TextView Yang Dicap Satu Jempol'), 10)

Mobile.swipe(300, 1000, 300, 100)

Mobile.swipe(300, 1000, 300, 50)

Mobile.waitForElementPresent(findTestObject('Eng/irenew/Data/TextView Get to Know Each Other'), 10)

Mobile.tap(findTestObject('Eng/irenew/Data/ImageView Get to Know Plus'), 0)

Mobile.tap(findTestObject('Eng/irenew/Data/ImageView Go on Date Plus'), 0)

