import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startExistingApplication('com.byu.id')

Mobile.waitForElementPresent(findTestObject('Eng/irenew/Pulsa/Credit Button'), 5)

Mobile.takeScreenshot('', FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Eng/irenew/Pulsa/Credit Button'), 0)

Mobile.takeScreenshot('', FailureHandling.CONTINUE_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Eng/irenew/Pulsa/Credit 20000'), 15)

Mobile.takeScreenshot('', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Eng/irenew/Pulsa/Credit 20000'), 0)

Mobile.waitForElementPresent(findTestObject('Eng/irenew/Payment/Result Payment'), 2)

Mobile.takeScreenshot('', FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/Eng/irenew/Payment/Toppingtab'), 0)

Mobile.waitForElementPresent(findTestObject('Eng/irenew/Topping/Disneyplus'), 2)

Mobile.takeScreenshot('', FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Eng/irenew/Topping/Disneyplus'), 0)

Mobile.waitForElementPresent(findTestObject('Eng/irenew/Topping/Disney 3GB'), 2)

Mobile.takeScreenshot('', FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Eng/irenew/Topping/Disney 3GB'), 0)

Mobile.takeScreenshot('', FailureHandling.CONTINUE_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Eng/irenew/Topping/Next Disney'), 0)

Mobile.tap(findTestObject('Eng/irenew/Topping/Next Disney'), 0)

Mobile.waitForElementPresent(findTestObject('Eng/irenew/Payment/Final Payment Page'), 3)

Mobile.closeApplication()

