import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.waitForElementPresent(findTestObject('Eng/irenew/Usage Details/TextView  See Details'), 5)

Mobile.tap(findTestObject('Eng/irenew/Usage Details/TextView  See Details'), 0)

Mobile.waitForElementPresent(findTestObject('Eng/irenew/Usage Details/TextView Total Data'), 5)

Mobile.takeScreenshot('', FailureHandling.STOP_ON_FAILURE)

Mobile.swipe(500, 800, 10, 800)

Mobile.waitForElementPresent(findTestObject('Eng/irenew/Usage Details/TextView Total Topping'), 5)

Mobile.takeScreenshot('', FailureHandling.STOP_ON_FAILURE)

Mobile.swipe(500, 800, 10, 800)

Mobile.waitForElementPresent(findTestObject('Eng/irenew/Usage Details/TextView Total Voice'), 5)

Mobile.takeScreenshot('', FailureHandling.STOP_ON_FAILURE)

Mobile.swipe(10, 800, 500, 800)

Mobile.waitForElementPresent(findTestObject('Eng/irenew/Usage Details/TextView Total Topping'), 5)

Mobile.takeScreenshot('', FailureHandling.STOP_ON_FAILURE)

