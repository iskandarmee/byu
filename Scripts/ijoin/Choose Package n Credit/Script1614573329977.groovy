import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startExistingApplication('com.byu.id', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Eng/ijoin/Creatne new Paket new'), 0)

Mobile.waitForElementPresent(findTestObject('Eng/ijoin/Package Choose Yang Bikin Dobel Kumat'), 0)

Mobile.takeScreenshot('', FailureHandling.STOP_ON_FAILURE)

Mobile.swipe(1000, 930, 500, 930)

Mobile.takeScreenshot('', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Eng/ijoin/TextView Choose Yang Bikin Cuan Tiap Hari'), 0)

Mobile.waitForElementPresent(findTestObject('Eng/ijoin/TextView 5.000 Topping Credits'), 0)

Mobile.tap(findTestObject('Eng/ijoin/TextView 5.000 Topping Credits'), 0)

Mobile.takeScreenshot('', FailureHandling.STOP_ON_FAILURE)

