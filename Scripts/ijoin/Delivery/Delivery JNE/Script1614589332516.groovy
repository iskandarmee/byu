import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startExistingApplication('com.byu.id')

Mobile.tap(findTestObject('Eng/ijoin/choosenumber1'), 0)

Mobile.tap(findTestObject('Eng/ijoin/Next get number new'), 0)

Mobile.tap(findTestObject('Object Repository/Eng/ijoin/Delivery Button'), 0)

Mobile.setText(findTestObject('Eng/ijoin/InputName Addres'), 'Ghosi', 0)

Mobile.setText(findTestObject('Eng/ijoin/Input Phonenumber Addres'), '087768644081', 0)

Mobile.scrollToText('Alamat')

Mobile.setText(findTestObject('Object Repository/Eng/ijoin/Input addres'), 'Kosan Momomyam', 0)

Mobile.tap(findTestObject('Object Repository/Eng/ijoin/Button Kecamatan'), 0)

Mobile.setText(findTestObject('Object Repository/Eng/ijoin/Input Kodepos'), '11480', 0)

Mobile.tap(findTestObject('Object Repository/Eng/ijoin/Find Poscode'), 0)

Mobile.tap(findTestObject('Object Repository/Eng/ijoin/Getbutton First'), 0)

Mobile.tap(findTestObject(''), 0)

Mobile.tap(findTestObject('Object Repository/Eng/ijoin/JNE checklist'), 0)

Mobile.tap(findTestObject('Object Repository/Eng/ijoin/Anteraja Checklist'), 0)

Mobile.tap(findTestObject('Object Repository/Eng/ijoin/Sicepat checklist'), 0)

Mobile.tap(findTestObject('Object Repository/Eng/ijoin/Next Delivery'), 0)

Mobile.closeApplication()

