package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object usernameGoogle
     
    /**
     * <p></p>
     */
    public static Object passwordGoogle
     
    /**
     * <p></p>
     */
    public static Object usernameFB
     
    /**
     * <p></p>
     */
    public static Object passwordFB
     
    /**
     * <p>Profile registered : Test</p>
     */
    public static Object Nomorhandphone
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            usernameGoogle = selectedVariables['usernameGoogle']
            passwordGoogle = selectedVariables['passwordGoogle']
            usernameFB = selectedVariables['usernameFB']
            passwordFB = selectedVariables['passwordFB']
            Nomorhandphone = selectedVariables['Nomorhandphone']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
