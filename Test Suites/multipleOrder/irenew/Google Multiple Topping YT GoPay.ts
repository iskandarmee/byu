<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Multiple Topping YT
Payment GoPay</description>
   <name>Google Multiple Topping YT GoPay</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>887be168-017e-4fdc-93b0-54e7e39a406d</testSuiteGuid>
   <testCaseLink>
      <guid>f7a54ece-1823-4b89-a35c-038e8d4f3655</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf0c9cd8-2eab-419a-b088-79a7771ed64e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Multiple/Topping YT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28b8bd0c-4cc5-4f87-82e5-9b6b438d4355</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/PGopay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4234eb95-dba3-40d6-b949-f4d5feb8a5b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef5c3a6d-4c6c-4c80-a214-f09d8f5eb3b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
