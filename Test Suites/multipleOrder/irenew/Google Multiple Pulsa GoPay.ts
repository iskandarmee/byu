<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Multiple Pulsa
Payment GoPay</description>
   <name>Google Multiple Pulsa GoPay</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>2c1dbfaa-7846-4ffe-9af7-8525ec64de89</testSuiteGuid>
   <testCaseLink>
      <guid>0ddb2fe0-ce9a-4781-b9af-caa298c7dd27</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e45473b-ba4a-4b43-a816-ff2bf52fa92b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Multiple/Pulsa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ad54cfc-175a-4b83-91c7-88fc8fbfbbad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/PGopay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8fddcbf1-df45-4a11-b13a-87b0b5e36058</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82f5d4a6-ce47-4ac7-92f2-10338520b09d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
