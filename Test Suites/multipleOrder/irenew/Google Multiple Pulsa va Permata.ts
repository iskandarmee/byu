<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Multiple Pulsa
Payment VA Permata</description>
   <name>Google Multiple Pulsa va Permata</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e2344998-e2b7-4987-bc17-4a6f5f2eb2f5</testSuiteGuid>
   <testCaseLink>
      <guid>67583054-1172-49e4-9d1e-03af08d6b378</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5ed5be1-61a1-41c3-869a-be18382dfcdc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Multiple/Pulsa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>333a9862-9724-4559-9d66-7d4c1aa16672</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/PvaPermataBank</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6675b566-b7d5-4a7d-a8e8-d1975389b173</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15aa3ee0-96e9-4cba-9bc4-fe353b189594</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
