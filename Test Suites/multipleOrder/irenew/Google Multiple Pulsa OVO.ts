<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Multiple Pulsa
Payment OVO</description>
   <name>Google Multiple Pulsa OVO</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>a7dcf592-e117-4963-8cbf-fb213ccf0419</testSuiteGuid>
   <testCaseLink>
      <guid>36743311-fb1f-440c-a2c8-de5d147c91e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abdb9b95-fea1-44a5-b457-710a7fd45b23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Multiple/Pulsa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6dd2c5b7-4da8-468c-a67c-a6d1ee6036aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/POVO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44820b9f-1af3-45fb-92ad-1eb2caf59975</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>054aa63c-d049-4651-afff-b0b7c05a31ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
