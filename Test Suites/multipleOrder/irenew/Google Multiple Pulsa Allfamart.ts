<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Multiple Pulsa
Payment Alfamart</description>
   <name>Google Multiple Pulsa Allfamart</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>8d3d92f4-bf21-43f0-b69e-5470680e1d33</testSuiteGuid>
   <testCaseLink>
      <guid>517b2661-a2e1-4e9a-a867-a94151dcdb8f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c776116-62fa-4adb-8fd9-cf25641357ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Multiple/Pulsa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>72ce7f98-aae2-45ba-975e-5f33ab27f8b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/PAlfamart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>170ac0fc-6bc0-4910-bc90-0fa2a472343e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee2dd811-e0ee-433f-84a0-98689628abbd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
