<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Multiple Pulsa
Payment Indomaret</description>
   <name>Google Multiple Pulsa Indomaret</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>db3fb11c-bd6f-437b-99a7-24042766aca4</testSuiteGuid>
   <testCaseLink>
      <guid>e6d06ecc-7ede-49e7-84e9-a800617bc9d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e97ec96-2eed-4e09-9825-5468e1760f3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Multiple/Pulsa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>71fa8d27-48e5-4091-bcaf-39d3823f5967</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/PIndomaret</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>138924e4-eb2c-4ff0-9068-268ccb9ece9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a387272-c629-43b8-a394-1eeb9a113628</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
