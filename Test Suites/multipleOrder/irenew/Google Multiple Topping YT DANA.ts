<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Multiple Topping YT
Payment DANA</description>
   <name>Google Multiple Topping YT DANA</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>a5b90ee2-247f-4689-b581-133298904ea2</testSuiteGuid>
   <testCaseLink>
      <guid>a1421323-e6eb-4e51-b930-9779864cee64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce2e52d0-0824-4512-ac2d-e9b0325f48f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Multiple/Topping YT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1ffc87a-3de4-4d8e-bb3a-15724c56659a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/PDana</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e434e76c-8c86-4e1f-ac0f-ecfd3164b2e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24161d18-287c-4d0f-9002-719107322d2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
