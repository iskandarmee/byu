<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Multiple Topping YT
Payment Alfamart</description>
   <name>Google Multiple Topping YT Alfamart</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>78f4de93-0745-45e1-96d4-d1b750b3abe0</testSuiteGuid>
   <testCaseLink>
      <guid>eb4585e5-7c70-4a60-8361-a4eda36b6f92</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5db36699-d279-4d1c-8f09-66f3beafa747</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Multiple/Topping YT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>698c43eb-89eb-403e-a330-2aca25730178</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/PAlfamart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf82cddc-6b52-488f-b0f3-03c563b6cda3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8196b1b-9413-4146-bb0e-b3210a701035</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
