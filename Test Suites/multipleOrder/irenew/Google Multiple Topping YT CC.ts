<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Multiple Topping YT
Payment CC</description>
   <name>Google Multiple Topping YT CC</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c8dcd0c3-c348-4c15-82a9-fb99c5e7a282</testSuiteGuid>
   <testCaseLink>
      <guid>69a0bf37-a65e-4627-a6f1-1984a8c9058e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3add0b6a-0e24-4cf8-9e03-8471b2969f24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Multiple/Topping YT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e82a03b-7276-4089-8ad2-bae0bd53ea0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/PCreditCard</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a314d6b-01cc-472b-bfe4-7262db78a243</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eafee72b-4c22-4c6b-ad14-0047023d2eb2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
