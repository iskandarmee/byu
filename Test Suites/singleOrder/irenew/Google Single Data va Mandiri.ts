<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Single Data
Payment va Mandiri</description>
   <name>Google Single Data va Mandiri</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>52e305c1-216b-41fc-834a-5e964e2d8952</testSuiteGuid>
   <testCaseLink>
      <guid>f9a55c3d-a2db-44f8-aa6b-bb67bff1606a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f35178b-2ccd-4891-81c8-1b245afb7099</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Single/Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ddfa55cc-02f7-4cf5-aca7-39a60469d2ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/PvaMandiri</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b669022-829a-4840-8079-e2821a40fd39</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7edc4b0c-f6ed-444f-9b41-3b76f55df7f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
