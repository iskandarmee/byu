<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Single Topping YouTube
Payment ShopeePay</description>
   <name>Google Single Topping ShopeePay</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>3b5a6361-906c-4604-81c1-1e20f8e8bd87</testSuiteGuid>
   <testCaseLink>
      <guid>4a5ec428-65c0-459b-ba74-7a9804c0d663</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d8f5eb6-7e59-472e-a74a-35a98f9ef6cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Single/Topping</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cdb0ade-cb43-4187-b7ac-711c546465a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/PShopeepay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>264c1ec3-131b-4ccb-8c1e-c053e7fff6ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04fb0310-b731-4855-a592-5117eb6efaf0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
