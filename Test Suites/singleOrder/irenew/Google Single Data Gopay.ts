<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Single Data
Payment Gopay</description>
   <name>Google Single Data Gopay</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ee013196-9b24-46cc-8a87-37907135d794</testSuiteGuid>
   <testCaseLink>
      <guid>ff44cded-248c-4879-8826-b13d8b1c417a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa89d41c-c0a8-485f-bdd9-37b3105d5832</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Single/Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c8db335-5a5d-4aa6-a77c-e2700dcefc20</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/PGopay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2f01756c-0ebb-4446-bb85-1855ff1760a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c18d6b90-38cc-41bc-bb28-15dea4e546fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
