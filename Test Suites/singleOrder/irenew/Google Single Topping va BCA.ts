<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Single Topping YouTube
Payment VA BCA</description>
   <name>Google Single Topping va BCA</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>337985b1-2076-457a-ba14-b7371bd718fc</testSuiteGuid>
   <testCaseLink>
      <guid>10fe5796-c50c-4ec5-8a95-3db49de50347</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ac4a6699-702d-4014-8a0d-5bc18fb9ef90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Single/Topping</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84f56c25-cd5b-43ac-93f3-874292229bc1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/PvaBCA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1460a19d-e90a-4652-831f-da8c2e132654</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8f5e7a8-f188-409f-a6c3-059aed3aa664</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
