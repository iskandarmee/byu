<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Single Data
Payment Alfamart</description>
   <name>Google Single Data Alfamart</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>6466aaf2-1d5a-4543-908e-b256c7c9f4c9</testSuiteGuid>
   <testCaseLink>
      <guid>5063c70b-1ed2-4b13-adea-b0a265823b8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd0b398b-56fa-4479-948d-722cbd191a76</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Single/Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>92ff5088-c2d9-437e-8146-7931c01311ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/PAlfamart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94a75708-d04a-4447-b71b-f251b9f042dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2dd24624-9021-44d5-9e9b-d86755b9e18d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
