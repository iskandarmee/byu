<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Single Topping YouTube
Payment OVO</description>
   <name>Google Single Topping OVO</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>3aee7a9e-6c37-47ee-9dcc-9f16576533f3</testSuiteGuid>
   <testCaseLink>
      <guid>61f0de23-2d19-4d60-8843-8fe9361d5503</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e731e7e7-5c2a-4ca3-ab71-e8201aa719de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Single/Topping</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6dab6463-34e9-410d-b573-ba9f6379e41c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/POVO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6b4e4b6-6266-4662-8eb4-869aa071dbe2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c015ae1a-92c1-4d5c-8b77-aba8d3cf0738</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
