<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Single Topping YouTube
Payment Gopay</description>
   <name>Google Single Topping Gopay</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>a3b0760d-7fb2-47b4-a439-cbb30b34867c</testSuiteGuid>
   <testCaseLink>
      <guid>f7a8319e-2c33-487f-bbdd-a2ffc457128e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0853b50-b380-4b87-9de8-5616e4269ba4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Single/Topping</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04a6b683-4441-4c97-b800-6fa19aed4750</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/PGopay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0943de29-fd47-4346-8932-134ec29706e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6bad915-b55e-42c2-811d-12f3e6cf5661</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
