<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Login Google
Buy Single Data
Payment VA BNI</description>
   <name>Google Single Data va BNI</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>d029751b-0ce4-446d-abda-52ef72461122</testSuiteGuid>
   <testCaseLink>
      <guid>46ad218e-768a-4800-9bdb-5726819cd554</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29dd5079-216a-4e47-9343-faf2d6caca4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Single/Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf685dc3-8b77-4633-a3c0-970a6705ff9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/PvaBNI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df3b9ac6-caf7-4d87-9306-f414798dcd1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43fb79a1-e3ea-49f4-ad0d-c22490e1b96e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
