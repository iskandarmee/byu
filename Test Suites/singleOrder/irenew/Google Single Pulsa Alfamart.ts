<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Google Single Pulsa Alfamart</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>69a14704-2c69-4ebb-bc23-5ea824fcc271</testSuiteGuid>
   <testCaseLink>
      <guid>7ab07408-de68-40f4-ad5b-fb38659f1af4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1ab7aa7b-a72b-412e-854c-a45c67a2eb7e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Single/Pulsa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4166b0d3-e2b6-4302-9fa4-1779bb6f8733</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/PAlfamart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0edbb94d-5636-40fb-8fe9-5af185fbbd55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telco/Notif Transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2842f7cf-b173-4272-b40f-42a8069bea04</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
